package info.bitcoinunlimited.voting.wallet.identity

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.navArgs
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import info.bitcoinunlimited.voting.databinding.FragmentIdentityBinding
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class IdentityFragment : Fragment(), IdentityView {
    private lateinit var binding: FragmentIdentityBinding
    private val intent = IdentityViewIntent()
    private val safeArgs: IdentityFragmentArgs by navArgs()

    @ExperimentalUnsignedTypes
    internal val viewModel: IdentityViewModel by activityViewModels {
        InjectorUtilsApp.provideIdentityViewModelFactory(
            this,
            UtilStringEncoding.hexToByteArray(safeArgs.privateKeyHex)
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentIdentityBinding.inflate(inflater, container, false)
        viewModel.bindIntents(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initClickListeners()
    }

    override fun onResume() {
        super.onResume()
        viewModel.initAddress()
        viewModel.viewModelScope.launch(Dispatchers.IO + viewModel.handler) {
            viewModel.fetchBalance()
            viewModel.subscribeAddress()
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.unsubscribeAddress()
    }

    private fun initClickListeners() {
        val address = viewModel.address.toString()
        binding.identityQrCode.setOnClickListener {
            copyAddressToClipBoard(address)
        }

        binding.addressLayout.setOnClickListener {
            copyAddressToClipBoard(address)
        }

        binding.layoutShareIdentity.setOnClickListener {
            shareAddress(address)
        }

        binding.viewVoteInBlockExplorerButtonIdentity.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.parse("https://explorer.bitcoinunlimited.info/address/$address")
            intent.data = uri
            startActivity(intent)
        }
    }

    internal fun copyAddressToClipBoard(address: String) {
        val clipboardManager = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", address)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(activity, "Address copied to clipboard", Toast.LENGTH_LONG).show()
    }

    private fun shareAddress(address: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, address)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    override fun initState() = intent.initState

    override fun render(state: IdentityViewState) {
        when (state) {
            is IdentityViewState.BalanceProgressbar -> { renderBalanceProgressbar(state) }
            is IdentityViewState.Identity -> { renderAddressState((state)) }
            is IdentityViewState.Balance -> { renderBalanceState(state) }
            is IdentityViewState.Problem -> { renderIdentityError(state) }
        }
    }

    private fun renderBalanceProgressbar(state: IdentityViewState.BalanceProgressbar) {
        if (state.enabled) {
            binding.loadingBalance.visibility = View.VISIBLE
        } else {
            binding.loadingBalance.visibility = View.GONE
        }
    }

    private fun renderAddressState(state: IdentityViewState.Identity) {
        binding.identityQrCode.visibility = View.VISIBLE
        binding.layoutShareIdentity.visibility = View.VISIBLE
        binding.identityAddress.text = state.address
        binding.identityQrCode.setImageBitmap(state.qrCode)
        binding.identityShareImage
    }

    @SuppressLint("SetTextI18n")
    private fun renderBalanceState(state: IdentityViewState.Balance) {
        binding.textCurrentBalance.text = "Balance: ${state.balanceHuman} BCH"
        if (state.balanceSatoshi > 0) {
            Toast.makeText(activity, "Fetched balance: ${state.balanceHuman} BCH", Toast.LENGTH_LONG).show()
        }
    }

    private fun renderIdentityError(state: IdentityViewState.Problem) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Error")
            .setMessage(state.exception.localizedMessage ?: state.exception.message)
            .setNeutralButton("ok") { _, _ -> }
            .show()
    }
}
