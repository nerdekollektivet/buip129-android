package info.bitcoinunlimited.voting.wallet.room

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "mnemonic_table")
data class Mnemonic(
    val phrase: String,
    @PrimaryKey
    val currentUserMnemonicTableId: String = "mnemonic"
) : Serializable {
    constructor() : this("INVALID")
}
