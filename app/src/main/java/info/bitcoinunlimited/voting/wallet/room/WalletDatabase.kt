package info.bitcoinunlimited.voting.wallet.room

import android.content.Context
import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.utils.Constants

class WalletDatabase(val context: Context) {
    private val addressIndex = 0

    suspend fun getVoterSecret(): Secret {
        val mnemonicOrWIF = MnemonicDatabase.getInstance(context).getMnemonic()
        return if (isWIF(mnemonicOrWIF)) {
            Log.d("DB", "Secret is a WIF")
            UnsecuredSecret(Key.decodePrivateKey(Constants.CURRENT_BLOCKCHAIN.v, mnemonicOrWIF))
        } else {
            Log.d("DB", "Secret is NOT a WIF")
            getSecretFromMnemonic(mnemonicOrWIF)
        }
    }

    suspend fun getVoterAddress(): PayDestination {
        return Pay2PubKeyHashDestination(Constants.CURRENT_BLOCKCHAIN, getVoterSecret())
    }

    companion object {
        fun isWIF(mnemonicOrWIP: String): Boolean {
            val regex = Regex("^[5KL][1-9A-HJ-NP-Za-km-z]{50,51}$")
            return regex.matches(mnemonicOrWIP)
        }

        // TODO: Improve mnemonic check. This just checks that it stats
        // with two words separated by space.
        fun isMnemonic(mnemonic: String): Boolean {
            val regex = Regex("^\\w+[ ]\\w+.*")
            return regex.matches(mnemonic)
        }
    }

    private fun getSecretFromMnemonic(mnemonic: String): Secret {
        val dummyPassCode = ""
        val seed = GenerateBip39Seed(mnemonic, dummyPassCode)
        return UnsecuredSecret(
            AddressDerivationKey.Hd44DeriveChildKey(
                seed,
                AddressDerivationKey.BIP44, AddressDerivationKey.ANY,
                0, 0, addressIndex
            )
        )
    }
}
