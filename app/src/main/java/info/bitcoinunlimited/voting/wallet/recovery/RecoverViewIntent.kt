package info.bitcoinunlimited.voting.wallet.recovery

import android.content.Context
import info.bitcoinunlimited.voting.utils.Event
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow

@ExperimentalCoroutinesApi
class RecoverViewIntent(
    val initState: MutableStateFlow<Event<Boolean>> = MutableStateFlow(Event(true)),
    val recoverMnemonic: MutableStateFlow<Event<RecoverMnemonic?>> = MutableStateFlow(Event(null))
) {
    data class RecoverMnemonic(
        val mnemonic: Mnemonic,
        val context: Context
    )
}
