package info.bitcoinunlimited.voting.wallet.identity

import info.bitcoinunlimited.voting.utils.Event
import info.bitcoinunlimited.voting.wallet.identity.IdentityViewIntent.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
interface IdentityView {
    /**
     * Intent to load the current Mnemonic state
     *
     * @return A flow that inits the current MnemonicViewState
     */
    fun initState(): Flow<Event<Boolean>>

    /**
     * Renders the ElectionDetailViewState
     *
     * @param state The current view state display
     */
    fun render(state: IdentityViewState)
}
