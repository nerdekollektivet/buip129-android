package info.bitcoinunlimited.voting.wallet.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MnemonicDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(mnemonic: Mnemonic)

    @Query("SELECT * FROM mnemonic_table WHERE currentUserMnemonicTableId == :mnemonicTableId")
    fun getMnemonic(mnemonicTableId: String): Mnemonic?
}
