package info.bitcoinunlimited.voting.utils

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import bitcoinunlimited.libbitcoincash.*
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.voting.VotingActivity
import info.bitcoinunlimited.voting.utils.VotingConstants.MNEMONIC_PHRASE
import info.bitcoinunlimited.voting.wallet.identity.IdentityViewModelFactory
import info.bitcoinunlimited.voting.wallet.mnemonic.MnemonicViewModel
import info.bitcoinunlimited.voting.wallet.room.Mnemonic
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@InternalCoroutinesApi
@ExperimentalCoroutinesApi
object InjectorUtilsApp {
    fun provideIdentityViewModelFactory(fragment: Fragment, privateKey: ByteArray): IdentityViewModelFactory {
        val payDestination: PayDestination = Pay2PubKeyHashDestination(ChainSelector.BCHMAINNET, UnsecuredSecret(privateKey))
        val address = payDestination.address ?: throw Exception("Cannot get payDestination.address")
        val electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)

        return IdentityViewModelFactory(fragment, electrumAPI, address)
    }

    fun provideMnemonicViewModelFactory(
        fragment: Fragment,
        privateKeyHex: String,
        mnemonic: Mnemonic,
    ): MnemonicViewModel.MMnemonicViewModelFactory {
        return MnemonicViewModel.MMnemonicViewModelFactory(privateKeyHex, mnemonic, fragment)
    }

    fun getVotingActivityIntent(applicationContext: Context, privateKey: Secret, intent: Intent, mnemonic: String): Intent {
        val electionId = intent.getStringExtra("election_id")
        return getVotePeerActivityIntent(applicationContext, privateKey, electionId, mnemonic)
    }

    internal fun getVotePeerActivityIntent(
        applicationContext: Context,
        privateKey: Secret,
        electionId: String?,
        mnemonic: String,
    ): Intent {
        return Intent(applicationContext, VotingActivity::class.java)
            .putExtra(MNEMONIC_PHRASE, mnemonic)
            .putExtra(Constants.PRIVATE_KEY, privateKey.getSecret())
            .putExtra("election_id", electionId)
            .putExtra("setSupportActionBar", false)
            .putExtra(Constants.SETUP_ACTION_BAR_WITH_NAV_CONTROLLER, false)
            .putExtra("setContentView", false)
            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }
}
