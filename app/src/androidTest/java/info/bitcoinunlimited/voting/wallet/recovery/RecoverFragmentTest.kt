package info.bitcoinunlimited.voting.wallet.recovery

import android.view.View
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import com.google.android.material.textfield.TextInputEditText
import info.bitcoinunlimited.voting.R
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Before
import org.junit.Test

@ExperimentalUnsignedTypes
@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class RecoverFragmentTest {
    companion object {
        init {
            val chain = ChainSelector.BCHMAINNET
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(chain.v)
        }
    }
    private lateinit var scenario: FragmentScenario<RecoverFragment>
    private lateinit var navController: TestNavHostController

    @Before
    fun setUp() = runBlockingTest {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        scenario = launchFragmentInContainer(null, R.style.AppTheme)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    private fun hasTextInputLayoutImportantForAutofillSetToNo(): Matcher<View> = object : TypeSafeMatcher<View>() {

        override fun describeTo(description: Description?) { }

        override fun matchesSafely(item: View?): Boolean {
            if (item !is TextInputEditText) return false

            return item.importantForAutofill == 2
        }
    }

    @Test
    fun recoveryErrorTest() {
        val errorMessage = "Enter backup phrase before submitting!"
        scenario.onFragment { fragment ->
            fragment.renderRecoveryError(RecoverViewState.RecoveryError(errorMessage))
        }

        onView(withText(errorMessage))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
    }

    @Test
    fun autoFillDisabledTest() {
        onView(withId(R.id.old_mnemonic_input))
            .check(matches(hasTextInputLayoutImportantForAutofillSetToNo()))
    }
}
