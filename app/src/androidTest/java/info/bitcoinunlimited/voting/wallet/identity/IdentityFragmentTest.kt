package info.bitcoinunlimited.voting.wallet.identity

import android.content.Context
import android.graphics.Bitmap
import androidx.core.os.bundleOf
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import bitcoinunlimited.libbitcoincash.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.votepeer.ElectrumAPI
import info.bitcoinunlimited.voting.R
import info.bitcoinunlimited.voting.VotePeerMock
import info.bitcoinunlimited.voting.utils.VotingConstants
import io.mockk.coEvery
import io.mockk.spyk
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class IdentityFragmentTest {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }
    private val privateKeyMock = VotePeerMock.mockPrivateKey()
    private val applicationContext = ApplicationProvider.getApplicationContext<Context>()
    private val mockResult = spyk(ElectrumClient.BalanceResult(1500000000, 1500000000))
    private val mockBalanceSatoshi = 300000000
    private val mockBalance = 3.0
    private val mockNoBalance = 0
    private lateinit var scenario: FragmentScenario<IdentityFragment>
    private lateinit var refreshBalance: String
    private lateinit var refreshingBalance: String
    private lateinit var shareIdentity: String
    private lateinit var qrCode: Bitmap
    private lateinit var addressMock: String
    private lateinit var viewModel: IdentityViewModel
    private lateinit var payAddressMock: PayAddress
    private lateinit var electrumAPI: ElectrumAPI

    @Before
    fun setUp() {
        val privateKeyHex = ToHexStr(privateKeyMock.getSecret()!!)
        val fragmentArgs = bundleOf(VotingConstants.privateKeyHex to privateKeyHex)
        scenario = launchFragmentInContainer(fragmentArgs, R.style.AppTheme)
        scenario.moveToState(Lifecycle.State.STARTED)
        refreshBalance = applicationContext.getString(R.string.refresh_balance)
        refreshingBalance = applicationContext.getString(R.string.fetching_bitcoin_cash_balance)
        shareIdentity = applicationContext.getString(R.string.share_identity)
        electrumAPI = spyk(ElectrumAPI.getInstance(ChainSelector.BCHMAINNET))
        payAddressMock = VotePeerMock.mockP2pkhAddress(ChainSelector.BCHMAINNET, privateKeyMock)
        addressMock = payAddressMock.toString()
        viewModel = spyk(IdentityViewModel(electrumAPI, payAddressMock))
        val qrHeight = 512
        val qrWidth = 512
        qrCode = viewModel.generateQrCode(addressMock, qrHeight, qrWidth)
    }

    @Test
    fun verifyInitialViewTest() = runBlockingTest {
        onView(withId(R.id.identity_address)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.identity_address)).check(matches(withText(addressMock)))
        onView(withId(R.id.identity_share_text)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.identity_share_text)).check(matches(withText(shareIdentity)))
        onView(withId(R.id.identity_share_image)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
    }

    @Test fun balanceProgressBarTest() {
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.BalanceProgressbar(true)
        }
        onView(withId(R.id.loading_balance)).check(matches(isDisplayed()))
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.BalanceProgressbar(false)
        }
        onView(withId(R.id.loading_balance)).check(matches(not(isDisplayed())))
    }

    @Test fun loadingBalanceTest() {
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.BalanceProgressbar(true)
            fragment.viewModel.state.value = IdentityViewState.Balance(mockNoBalance)
        }
        onView(withId(R.id.loading_balance)).check(matches(isDisplayed()))
        onView(withId(R.id.text_current_balance)).check(matches(withText("Balance: ${mockNoBalance.toDouble()} BCH")))
    }

    @Test fun notLoadingNoBalanceTest() {
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.BalanceProgressbar(false)
            fragment.viewModel.state.value = IdentityViewState.Balance(0)
        }
        onView(withId(R.id.loading_balance)).check(matches(not(isDisplayed())))
        onView(withId(R.id.text_current_balance)).check(matches(withText("Balance: ${mockNoBalance.toDouble()} BCH")))
    }

    @Test fun hasBalanceTest() = runBlockingTest {
        coEvery { electrumAPI.getBalance(payAddressMock) } returns mockResult
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.Balance(mockBalanceSatoshi)
        }
        onView(withId(R.id.identity_share_image)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.text_current_balance)).check(matches(withText("Balance: $mockBalance BCH")))
    }

    @Test fun loadingCompletedTest() = runBlockingTest {
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.BalanceProgressbar(false)
            fragment.viewModel.state.value = IdentityViewState.Balance(mockBalanceSatoshi)
            fragment.viewModel.state.value = IdentityViewState.Identity(addressMock, qrCode)
        }
        onView(withId(R.id.loading_balance)).check(matches(not(isDisplayed())))
        onView(withId(R.id.identity_share_image)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withId(R.id.text_current_balance)).check(matches(withText("Balance: $mockBalance BCH")))

        onView(withId(R.id.identity_address)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_address)).check(matches(withText(addressMock)))
        onView(withId(R.id.identity_share_text)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_text)).check(matches(withText(shareIdentity)))
        onView(withId(R.id.identity_qr_code)).check(matches(isDisplayed()))
    }

    @Test fun identityErrorTest() = runBlockingTest {
        val errorMessage = "Something went wrong! Oh no."
        val errorDialogTitle = "Error"
        val neutralButtonText = "ok"
        scenario.onFragment { fragment ->
            fragment.viewModel.state.value = IdentityViewState.Problem(Error(errorMessage))
        }

        onView(withText(errorMessage)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withText(errorDialogTitle)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withText(neutralButtonText)).check(matches(withEffectiveVisibility(Visibility.VISIBLE)))
        onView(withText("ok")).perform(click())
        onView(withText(errorMessage)).check(doesNotExist())
        onView(withText(errorDialogTitle)).check(doesNotExist())
        onView(withText(neutralButtonText)).check(doesNotExist())
    }

    @Test fun copyAddressToClipBoardTest() {
        scenario.onFragment { fragment ->
            fragment.copyAddressToClipBoard(addressMock)
        }
    }
}
