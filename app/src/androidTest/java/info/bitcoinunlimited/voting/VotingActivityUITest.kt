package info.bitcoinunlimited.voting

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.* // ktlint-disable no-wildcard-imports
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.matcher.ViewMatchers.* // ktlint-disable no-wildcard-imports
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.votepeer.utils.Constants
import info.bitcoinunlimited.voting.utils.InjectorUtilsApp
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.* // ktlint-disable no-wildcard-imports
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Test

@DelicateCoroutinesApi
@ExperimentalUnsignedTypes
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class VotingActivityUITest {
    private val intent = InjectorUtilsApp.getVotingActivityIntent(
        getApplicationContext(), VotePeerMock.mockPrivateKey(), mockk(relaxed = true), "mock"
    )

    private lateinit var scenario: ActivityScenario<VotingActivity>

    @Before
    fun setUp() = runBlockingTest {
        mockkStatic(Firebase::class)
        mockkStatic(FirebaseApp::class)
        mockkStatic(FirebaseAuth::class)
        mockkStatic(FirebaseFunctions::class)
        every { FirebaseAuth.getInstance() } returns mockk(relaxed = true)
        every { FirebaseFunctions.getInstance(Constants.region) } returns mockk(relaxed = true)
        scenario = launchActivity(intent)
        scenario.moveToState(Lifecycle.State.RESUMED)
    }

    @After
    fun clear() = runBlockingTest {
        scenario.moveToState(Lifecycle.State.DESTROYED)
        clearAllMocks()
    }

    @Test
    fun open_menu_drawer_display_nav_items() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .check(matches(isClosed(Gravity.LEFT)))
            .perform(DrawerActions.open())

        onView(withId(R.id.menu_election_master)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_nav_login)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_nav_identity)).check(matches(isDisplayed()))
        onView(withId(R.id.menu_nav_mnemonic)).check(matches(isDisplayed()))
    }

    @Test fun open_menu_drawer_navigate_to_election_master() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_login))
            .perform(click())
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_election_master))
            .perform(click())

        onView(withId(R.id.election_master_fragment_layout)).check(matches(isDisplayed()))
    }

    @Test
    fun open_menu_drawer_navigate_to_login() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_login))
            .perform(click())

        onView(withId(R.id.login_fragment)).check(matches(isDisplayed()))
        onView(withId(R.id.website_login_qr_scanner)).check(matches(isDisplayed())).check(matches(isEnabled()))
    }

    @Test fun open_menu_drawer_navigate_to_login_start_qr_scanner() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_login))
            .perform(click())
        // onView(withId(R.id.website_login_qr_scanner)).perform(click())
        // Fails CI instrumented tests
        // ref: https://gitlab.com/jQrgen/buip129-android/-/jobs/3072304981#L4634
        // TODO: Implement permission test
        // onView(withText("Place a barcode inside the viewfinder rectangle to scan it.")).check(matches(isDisplayed()))
    }

    @Test
    fun navigate_to_identity_test() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_identity))
            .perform(click())

        onView(withId(R.id.address_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_qr_code)).check(matches(isDisplayed()))
        onView(withId(R.id.text_current_balance)).check(matches(isDisplayed()))
        onView(withId(R.id.layout_share_identity)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_image)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_text)).check(matches(isDisplayed()))
    }

    @Test fun navigate_to_identity_login_identity_test() {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_identity))
            .perform(click())
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_login))
            .perform(click())
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_identity))
            .perform(click())

        onView(withId(R.id.address_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_qr_code)).check(matches(isDisplayed()))
        onView(withId(R.id.text_current_balance)).check(matches(isDisplayed()))
        onView(withId(R.id.layout_share_identity)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_image)).check(matches(isDisplayed()))
        onView(withId(R.id.identity_share_text)).check(matches(isDisplayed()))
    }

    @Test
    fun navigate_to_mnemonic() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_mnemonic))
            .perform(click())

        onView(withId(R.id.mnemonic_headline)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_display)).check(matches(isDisplayed()))
        onView(withId(R.id.recover_mnemonic_button)).check(matches(isDisplayed()))
        onView(withId(R.id.mnemonic_error_message)).check(matches(isDisplayed()))
    }

    @Test
    fun navigate_to_recover() = runBlockingTest {
        onView(withId(R.id.drawer_layout))
            .perform(DrawerActions.open())
        onView(withId(R.id.menu_nav_mnemonic))
            .perform(click())
        onView(withId(R.id.recover_mnemonic_button))
            .perform(click())
    }

    @Test
    fun accept_camera_permission_test() = runBlockingTest {
        val appCompatImageButton = onView(
            Matchers.allOf(
                withContentDescription("Open navigation drawer"),
                childAtPosition(
                    Matchers.allOf(
                        withId(R.id.toolbar),
                        childAtPosition(
                            withClassName(Matchers.`is`("com.google.android.material.appbar.AppBarLayout")),
                            0
                        )
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        appCompatImageButton.perform(click())

        val navigationMenuItemView = onView(
            Matchers.allOf(
                withId(R.id.menu_nav_login),
                childAtPosition(
                    Matchers.allOf(
                        withId(R.id.design_navigation_view),
                        childAtPosition(
                            withId(R.id.nav_view),
                            0
                        )
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        navigationMenuItemView.perform(click())

//        val materialButton = onView(
//            Matchers.allOf(
//                withId(R.id.website_login_qr_scanner), withText("Login"),
//                childAtPosition(
//                    childAtPosition(
//                        withId(R.id.nav_host_fragment),
//                        0
//                    ),
//                    2
//                ),
//                isDisplayed()
//            )
//        )
        // materialButton.perform(click())
        // Fails CI instrumented tests
        // ref: https://gitlab.com/jQrgen/buip129-android/-/jobs/3072304981#L4634
        // TODO: Implement permission test
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>,
        position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent) &&
                    view == parent.getChildAt(position)
            }
        }
    }

    @Test fun same_destination_clicked() {
        // TODO: Implement. See VotingActivity.sameDestinationClicked()
    }

    @Test
    fun is_offline() {
        // TODO: Implement with VotePeerActivityViewState.ConnectionStatus
    }

    @Test
    fun is_online() {
        // TODO: Implement with VotePeerActivityViewState.ConenctionStatus
    }

    @Test
    fun displays_error() {
        // TODO: Implement with VotePeerActivityState.VotePeerActivityError
    }
}
